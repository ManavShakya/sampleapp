package com.example.fragmentexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MessageFragment.OnMessageRead {
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(findViewById(R.id.fragment_container)!=null){
            if(savedInstanceState!=null)
                return;
        }
        textView = findViewById(R.id.txt_display_message);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container,new MessageFragment(),null)
                .commit();
    }

    @Override
    public void readMessage(String message, int it) {
      try{
          textView.setText(message + it);
      }catch(Exception e){
          Log.e("manav", "fatt gya");
      }

    }
}
